#!/usr/bin/guile -s
!#

(use-modules (srfi srfi-1))

(define commands "
Available conversions:

kg   -> lbs
lbs  -> kg
cm   -> inch
inch -> cm
")

;; Early exit if there aren't the correct number of arguments
(if (not (= 3 (length (cdr (command-line)))))
    ((display
      (string-append "Please use command line arguments.

Example:
convert.scm kb lbs 170
" commands)) (newline) (quit)))

;; These are the available functions
(define lbs_to_kg
  (lambda (m)
    (/ m 2.2046)))

(define kg_to_lbs
  (lambda (m)
    (* m 2.2046)))

(define inch_to_cm
  (lambda (m)
    (* m 2.54)))

(define cm_to_inch
  (lambda (m)
    (/ m 2.54)))

;; This is a hash table where the string maps to a function
(define h (make-hash-table))
(hash-set! h "lbs_to_kg" lbs_to_kg)
(hash-set! h "kg_to_lbs" kg_to_lbs)
(hash-set! h "inch_to_cm" inch_to_cm)
(hash-set! h "cm_to_inch" cm_to_inch)

(define from (second (command-line)))
(define to (third (command-line)))
(define func (hash-ref h (string-append from "_to_" to)))

(define convert
  (lambda (num)
    (if (eqv? func #f)
	(string-append "Sorry, conversion could not be done.\n" commands)
	(func num))))

(display (convert (string->number (fourth (command-line)))))

(newline)
