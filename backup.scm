#!/usr/bin/guile -s
!#

(define files '("~/org drive:Dots/org"
		"~/Music drive:Music"
		"~/Books drive:Books"))

(define backup
  (lambda (file)
    (system (string-append "rclone sync " file))
    (display (string-append (car (string-split file #\space)) " backed up\n"))))

(for-each backup files)

(display "Files backed up.")

(newline)
