#! /usr/bin/guile -s
!#

(use-modules (srfi srfi-1))

(define song (second (command-line)))
(define command-string (string-append "youtube-dl --extract-audio --audio-format mp3 " song))

;; This assigns dir to either the third command-line argument, or ~/Downloads if that isn't possible.
(define dir (if (> (length (command-line)) 2)
		(third (command-line))
		"~/Downloads"))

(system (string-append "cd " dir " && " command-string " && cd -"))
