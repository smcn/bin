#! /usr/bin/env ruby

class Volume
  def up
    system(change_volume('+'))
  end

  def down
    system(change_volume('-'))
  end end

# uses pulseaudio to control volume
class LinuxVolume < Volume
  def initialize(step = 5)
    @step = step
  end

  def toggle_mute
    `amixer sset Master toggle`
  end

  private

  def change_volume(dir)
    "amixer -D pulse sset Master #{@step}%#{dir},#{@step}%#{dir} unmute"
  end
end

# uses mixerctl to control volume
class OpenBSDVolume < Volume
  def initialize(step = 10)
    @step    = step
  end

  def toggle_mute
    `mixerctl outputs.master.mute=toggle`
  end

  private

  def current_volume
    `mixerctl outputs.master`.split(',')[-1].strip
  end

  def change_volume(dir)
    toggle_mute if `mixerctl outputs.master.mute` =~ /on/
    new_vol = eval("#{current_volume} #{dir} #{@step}")
    "mixerctl outputs.master=#{new_vol},#{new_vol}"
  end
end

# uses mixer to control volume
class FreeBSDVolume < Volume
  def initialize(step = 5)
    @step    = step
  end

  def toggle_mute
    `mixer vol mute`
  end

  private

  def current_volume
    `mixer`.split("\n")[0].split(":")[1]
  end

  def change_volume(dir)
    new_vol = eval("#{current_volume} #{dir} #{@step}")
    `mixer vol #{new_vol}`
  end
end

vol = `uname -a` =~ /OpenBSD/ ? OpenBSDVolume.new : `uname -a` =~ /FreeBSD/ ? FreeBSDVolume.new : LinuxVolume.new

case ARGV[0]
when 'up'
  vol.up
when 'down'
  vol.down
when 'toggle_mute'
  vol.toggle_mute
end

# `dwm-bar.rb`
